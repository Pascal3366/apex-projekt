/**
 *
 * @file: dbconfig.js
 * @author: Pascal Peinecke
 *
*/

'use strict';

// store database credentials

/**
 * user          - the username used for the connection
 * password      - the password used for the connection
 * connectString - the connection string provided to connect to as specific database server
 */
export default {
  user: "system",
  password: "datadata1337!",
  connectString: "172.17.0.2/ORCLCDB"
};
