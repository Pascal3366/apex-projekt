/**
 *
 * @file: routes.js
 * @author: Pascal Peinecke
 *
*/

'use strict';

import path from 'path'; // Module providing access to the host filesystem
import mime from 'mime';
import fs from 'fs';
import async from 'async';
import chalk from 'chalk';
import options from '../../options.json';
import { FormatQuery } from '../dojsonquery'; // include function to save database table content in json object

export default function (app) {

  app.post('/get/table', (req, res) => {
    // let express wait for the event /get/table
    let query = req.body;

    let doc_path = options.path;
    let filename = query.table + ".xlsx"; // specify the filename for the output
    let filelocation = doc_path + filename;
    let mimetype = mime.lookup(filelocation);
    let downloadfilelocation = path.resolve(__dirname + '/../../outputs') + "/" + filename;

    /**
     * @name runFormatQry
     * @description runs the sql query.
     * @param {string} query - The query to be used.
     * @param {string} filelocation - The output file location.
     * @return {Promise} Promise - return a promise to handle async io
     */
    let runFormatQry = function (query, filelocation) {
      return new Promise((resolve, reject) => {
        // reject and resolve are functions provided by the Promise
        // implementation. Call only one of them.
        // Do your logic here - you can do WTF you want.:)
        console.log(chalk.blue('INFO: executing format query now!'));
        console.log(chalk.blue('SQL Query String: ' + query.queryString));
        FormatQuery(query, filelocation, (err, result) => {
          // PS. Fail fast! Handle errors first, then move to the
          // important stuff (that's a good practice at least)
          if (err) {
            // Reject the Promise with an error
            console.log(chalk.red('ERROR running formatQry: ' + err));
            return reject(err);
          }

          // Resolve (or fulfill) the promise with data
          return resolve(result);
        });
      });
    };

    /**
     * @name sendDownloadRequest
     * @description sends the download request to the browser
     * @param {string} downloadfilelocation - Specify the location of the generated file to download
     * @return {Promise} Promise - return a promise to handle async io
     */
    let sendDownloadRequest = function (downloadfilelocation) {
      return new Promise((resolve, reject) => {
        let filename = path.basename(downloadfilelocation);
        let mimetype = mime.lookup(downloadfilelocation);
        res.setHeader('Content-disposition', 'attachment; filename=' + filename);
        res.setHeader('Content-type', mimetype);
        res.download(downloadfilelocation);
      });
    };

    async.waterfall([
      function () {
        runFormatQry(query, filelocation);
      }, function () {
        sendDownloadRequest(downloadfilelocation);
      }], function (err, result) {
      // result now equals 'done'
      if(err) {
        console.log(chalk.red(err));
        return reject(err);
      } else {
        console.log(chalk.green('all done.'));
      }
    });

  });

  // clean all generated files
  app.get('/clean', (req, res) => {
    const options = require('../../options.json');
    const filepath = options.path;
    const del = require('node-delete');
    del([filepath + '/*'], function (err, paths) {
      res.write('success');
      res.end();
    });
  });

};
