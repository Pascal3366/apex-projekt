/**
 *
 * @file: dojsonquery.js
 * @author: Pascal Peinecke
 *
*/

'use strict';

import { generateWithFormat } from './worksheet'; // import function generateWithFormat from worksheet
import dbConfig from './config/dbconfig.js'; // include database login credentials
import oracledb from 'oracledb'; // Module providing database access for the oracle database
import chalk from 'chalk'; // Module for prettier terminal log

  /**
   * @name FormatQuery
   * @description Execute sql query and collect data from the table.
   * @param {string} table - The table to be used.
   * @param {string} filelocation - The absolute filename for the generated file.
   * @param {string} format - Specify formatting options
   * @return {Promise} Promise - return a promise to handle async io
   */
  export function FormatQuery(query, filelocation) {

    return new Promise((resolve, reject) => {
      // reject and resolve are functions provided by the Promise
      // implementation. Call only one of them.

      oracledb.getConnection({ // execute the database connection handler
          user: dbConfig.user, // specify the username used for the database connection
          password: dbConfig.password, // specify the password used for the database connection
          connectString: dbConfig.connectString // specify the connection string containig the database server to connect to
        },
        function(error, connection) {
          if (error) {
            // handle connection error
            console.log(chalk.red(error.message));
            return reject(error);
          }

          if (connection.oracleServerVersion < 1201000200) { // check for the correct oracle database server version
            console.error(chalk.red('This program only works with Oracle Database 12.1.0.2 or greater')); // handle incompatibility error
          } else {
            console.log(chalk.green('Connection was successful!'));

            console.log(chalk.blue('ab hier beginnt die query... mit formatoptionen'));
            console.log(chalk.blue('Führe query aus... ' + query.queryString));
            connection.execute(
              query.queryString,
              function(error, result) {
                if (error) {
                  console.error(chalk.red(error.message));
                  console.log(chalk.red('Fehler beim ausführen der json query'));
                  return reject(error);
                } else {
                  console.log(chalk.green('Query erfolgreich.'));
                  var sheet_data = result.rows;
                  var sheet_meta = result.metaData;

                  generateWithFormat(query, sheet_meta, sheet_data, filelocation); // Execute the generation of the excel file
                }
              });

          }

        });
      // Resolve (or fulfill) the promise with data
      return resolve(result);
      callback(result);
    })
  }
